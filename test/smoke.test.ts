import { test } from '@playwright/test';

test.setTimeout(35e3);

test('go to /', async ({ page }) => {
  page.setDefaultTimeout(30000);

  await page.goto('/');

  await page.waitForSelector(`text=Home Page`);
});
