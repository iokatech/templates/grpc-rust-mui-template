// @generated automatically by Diesel CLI.

diesel::table! {
    users (id) {
        id -> Integer,
        username -> Text,
        first_name -> Text,
        last_name -> Text,
        email -> Text,
        address -> Text,
        city -> Text,
        password -> Text,
    }
}
