use self::models::*;
use diesel::prelude::*;
use http::Method;
use server::*;
use tokio::sync::broadcast;
use tokio::sync::mpsc;
use tokio_stream::wrappers::ReceiverStream;
use tonic::{transport::Server, Response, Status};
use tonic_web::GrpcWebLayer;
use tower_http::cors::{AllowOrigin, Any, CorsLayer};
use users_pb::user_service_server::{UserService, UserServiceServer};
use users_pb::{
    CreateUserRequest, CreateUserResponse, DeleteUserRequest, DeleteUserResponse, GetUserRequest,
    GetUserResponse, ListUsersRequest, ListUsersResponse, OnUserEventsRequest,
    OnUserEventsResponse, UpdateUserRequest, UpdateUserResponse, User, UserEventType,
};

pub mod users_pb {
    include!("../api/users.v1.rs");
}

pub struct MyUserService {
    tx: broadcast::Sender<Result<OnUserEventsResponse, Status>>,
}

impl Default for MyUserService {
    fn default() -> MyUserService {
        MyUserService {
            tx: broadcast::channel::<Result<OnUserEventsResponse, Status>>(20).0,
        }
    }
}

#[tonic::async_trait]
impl UserService for MyUserService {
    type OnUserEventsStream = ReceiverStream<Result<OnUserEventsResponse, Status>>;

    async fn on_user_events(
        &self,
        _request: tonic::Request<OnUserEventsRequest>,
    ) -> Result<Response<Self::OnUserEventsStream>, Status> {
        let mut broadcast_rx = self.tx.subscribe();
        let (tx, rx) = mpsc::channel::<Result<OnUserEventsResponse, Status>>(1);

        tokio::spawn(async move {
            loop {
                let msg = broadcast_rx.recv().await;
                if msg.is_err() {
                    break;
                }
                let send_result = tx.send(msg.unwrap()).await;
                if send_result.is_err() {
                    break;
                }
            }
        });

        Ok(Response::new(ReceiverStream::new(rx)))
    }

    async fn list_users(
        &self,
        _request: tonic::Request<ListUsersRequest>,
    ) -> Result<tonic::Response<ListUsersResponse>, tonic::Status> {
        use self::schema::users::dsl::*;

        let connection = &mut establish_connection();

        let result = users.load::<Users>(connection);

        if result.is_err() {
            return Err(Status::internal("Failed to load users"));
        }

        let rows = result.unwrap();

        let mut ret: Vec<User> = Vec::new();

        for result in rows {
            ret.push({
                User {
                    id: Some(result.id),
                    username: result.username,
                    email: Some(result.email),
                    password: Some(result.password),
                    first_name: Some(result.first_name),
                    last_name: Some(result.last_name),
                    address: Some(result.address),
                    city: Some(result.city),
                }
            })
        }

        let reply: ListUsersResponse = ListUsersResponse { users: ret };
        Ok(Response::new(reply))
    }

    async fn create_user(
        &self,
        request: tonic::Request<CreateUserRequest>,
    ) -> Result<tonic::Response<CreateUserResponse>, tonic::Status> {
        let message = request.get_ref();
        let user = message.user.as_ref().unwrap();

        use crate::schema::users;

        let connection = &mut establish_connection();

        let new_user = NewUser {
            username: &user.username,
            first_name: user.first_name(),
            last_name: user.last_name(),
            email: user.email(),
            address: "",
            city: "",
            password: "",
        };

        let result = diesel::insert_into(users::table)
            .values(&new_user)
            .execute(connection);

        if result.is_err() {
            return Err(Status::internal(result.unwrap_err().to_string()));
        }

        let _result = self.tx.send(Ok(OnUserEventsResponse {
            event_type: UserEventType::Created.into(),
        }));

        Ok(Response::new(CreateUserResponse {
            user: Some(user.to_owned()),
        }))
    }

    ///
    async fn get_user(
        &self,
        _request: tonic::Request<GetUserRequest>,
    ) -> Result<tonic::Response<GetUserResponse>, tonic::Status> {
        unimplemented!();
    }

    ///
    async fn update_user(
        &self,
        _request: tonic::Request<UpdateUserRequest>,
    ) -> Result<tonic::Response<UpdateUserResponse>, tonic::Status> {
        unimplemented!();
    }

    ///
    async fn delete_user(
        &self,
        request: tonic::Request<DeleteUserRequest>,
    ) -> Result<tonic::Response<DeleteUserResponse>, tonic::Status> {
        use self::schema::users::dsl::*;

        let message = request.get_ref();

        let id_to_delete: i32 = message.id.parse().unwrap_or(0);

        let connection = &mut establish_connection();

        let result = diesel::delete(users.filter(id.eq(id_to_delete))).execute(connection);
        if result.is_err() {
            return Err(Status::internal("Failed to delete message"));
        }

        self.tx
            .send(Ok(OnUserEventsResponse {
                event_type: UserEventType::Deleted.into(),
            }))
            .unwrap_or(0);

        Ok(Response::new(DeleteUserResponse {}))
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let addr = "[::1]:8000".parse()?;
    let user_service = UserServiceServer::new(MyUserService::default());

    let cors = CorsLayer::new()
        // allow any headers
        .allow_headers(Any)
        // allow `POST` when accessing the resource
        .allow_methods([Method::POST])
        // allow requests from below origins
        .allow_origin(AllowOrigin::any());

    Server::builder()
        .accept_http1(true)
        .layer(cors)
        .layer(GrpcWebLayer::new())
        .add_service(user_service)
        .serve(addr)
        .await?;

    Ok(())
}
