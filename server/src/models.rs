use crate::schema::users;
use diesel::prelude::*;

#[derive(Queryable)]
pub struct Users {
    pub id: i32,
    pub username: String,
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub address: String,
    pub city: String,
    pub password: String,
}

#[derive(Insertable)]
#[diesel(table_name = users)]
pub struct NewUser<'a> {
    pub username: &'a str,
    pub first_name: &'a str,
    pub last_name: &'a str,
    pub email: &'a str,
    pub address: &'a str,
    pub city: &'a str,
    pub password: &'a str,
}
