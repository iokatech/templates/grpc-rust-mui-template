import {
  createGrpcWebTransport,
  createPromiseClient,
} from '@bufbuild/connect-web';
import { UserService } from '../../api/users/v1/users_connectweb';

// This transport is going to be used throughout the app
const transport = createGrpcWebTransport({
  baseUrl: 'http://localhost:8000',
});

export const client = createPromiseClient(UserService, transport);
