import { makeObservable, action, observable } from 'mobx';
import { User } from '../../api/users/v1/users_pb';
import { client } from '../utils/client';

export class UsersStore {
  users: User[] = observable([]);
  constructor() {
    makeObservable(this, {
      users: observable,
      addUser: action,
      deleteUser: action,
      listAllUsers: action,
      setUsers: action,
    });

    this.watchUserEvents();
  }

  watchUserEvents = async () => {
    for await (const event of client.onUserEvents({})) {
      console.log('Received event response', event);
      this.listAllUsers();
    }
  };

  setUsers = (users: User[]) => {
    this.users = [...users];
  };

  /**
   * Fetch all users.
   *
   */
  listAllUsers = async () => {
    const resp = await client.listUsers({});
    this.setUsers(resp.users);
  };

  /**
   * Add a new user.
   *
   * @param user this is the user to add.
   */
  addUser = async (user: User) => {
    return await client.createUser({ user });
  };

  /**
   * Delete a user.
   *
   * @param user_id this is the unique id of the user to delete.
   */
  deleteUser = async (user_id: number) => {
    return await client.deleteUser({ id: user_id.toString() });
  };
}
