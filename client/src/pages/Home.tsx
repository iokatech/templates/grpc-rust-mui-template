import React, { useEffect } from 'react';
import { Button, Container, IconButton, Link, Typography } from '@mui/material';
import { Add, Delete } from '@mui/icons-material';
import {
  DataGrid,
  GridColDef,
  GridValueGetterParams,
  GridToolbarContainer,
  GridToolbarColumnsButton,
  GridToolbarDensitySelector,
} from '@mui/x-data-grid';
import { faker } from '@faker-js/faker';
import { observer } from 'mobx-react-lite';
import rootStore from '../stores';
import * as users_pb from '../../api/users/v1/users_pb';

const HomePage: React.FC = observer(() => {
  const { users, deleteUser, addUser, listAllUsers } = rootStore.usersStore;

  // On page render, fetch all the users
  useEffect(() => {
    listAllUsers();
  }, [listAllUsers]);

  // Handle the user delete button being clicked
  const onDelete = (user: users_pb.User) => {
    deleteUser(user.id || 0);
  };

  // Handle the add user tool being clicked - just add a user with fake data
  const onAddUser = () => {
    const firstName = faker.name.firstName();
    const lastName = faker.name.lastName();
    const userName = `${firstName}.${lastName}`;

    const user = new users_pb.User({
      email: `${userName}@fake.com`,
      firstName: firstName,
      lastName: lastName,
      username: userName,
    });

    addUser(user);
  };

  const columns: GridColDef[] = [
    { field: 'id', headerName: 'ID', width: 70 },
    { field: 'firstName', headerName: 'First name', width: 130 },
    { field: 'lastName', headerName: 'Last name', width: 130 },
    {
      field: 'fullName',
      headerName: 'Full name',
      description: 'This column has a value getter and is not sortable.',
      sortable: false,
      width: 160,
      valueGetter: (params: GridValueGetterParams) =>
        `${params.row.firstName || ''} ${params.row.lastName || ''}`,
    },
    { field: 'email', headerName: 'Email', width: 250 },
    { field: 'username', headerName: 'Username', width: 130 },
    {
      field: 'delete',
      headerName: '',
      sortable: false,
      filterable: false,
      renderCell: (params: { row: users_pb.User }) => {
        return (
          <IconButton onClick={() => onDelete(params.row)}>
            <Delete />
          </IconButton>
        );
      },
    },
  ];

  const customToolbar = () => {
    return (
      <GridToolbarContainer>
        <GridToolbarColumnsButton />
        <GridToolbarDensitySelector />
        <Button startIcon={<Add />} onClick={onAddUser}>
          Add User
        </Button>
      </GridToolbarContainer>
    );
  };

  return (
    <Container maxWidth="lg">
      <Typography variant="h2">Home Page</Typography>
      <Typography variant="body2">
        Open multiple browsers to{' '}
        <Link target="_blank" href="http://localhost:3000">
          home page
        </Link>{' '}
        and see each window update as you add/remove users.
      </Typography>
      <br />
      <div style={{ height: 800, width: '100%' }}>
        <DataGrid<users_pb.User>
          rows={[...users]}
          columns={columns}
          pageSize={50}
          rowsPerPageOptions={[5, 25, 50]}
          checkboxSelection
          components={{
            Toolbar: customToolbar,
          }}
        />
      </div>
    </Container>
  );
});

export default HomePage;
